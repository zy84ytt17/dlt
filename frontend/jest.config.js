module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel', //  支持ts
  testMatch: [
    "**/tests/unit/**/**.spec.js"
  ],  // 需要执行哪些目录下的测试用例   【也可修改** 单独测试某个文件】
  collectCoverage: true, // 或者通过 --coverage 参数来调用 Jest, 收集覆盖率
  coverageReporters: ["html", "text-summary"],  //  覆盖率报告的格式， "text-summary"就是terminal 里的总结
  coverageDirectory: "tests/unit/report"  //  输出覆盖信息文件的目录
}
