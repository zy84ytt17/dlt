
const isProduction = process.env.NODE_ENV === 'production'
const plugins = [
  '@babel/plugin-proposal-nullish-coalescing-operator', 
  '@babel/plugin-proposal-optional-chaining'
]
if (isProduction) {
  plugins.push(['transform-remove-console', { 'exclude': ['error', 'warn'] }])
}

module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  plugins: plugins
}
