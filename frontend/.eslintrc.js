module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'airbnb-typescript/base',
    'plugin:unicorn/recommended',
    'plugin:vue/recommended',
    'prettier',
    'prettier/@typescript-eslint',
    'prettier/unicorn',
    'prettier/vue'
  ],
  parser: "vue-eslint-parser",
  parserOptions: {
    parser: "@typescript-eslint/parser",
    ecmaVersion: 2020,
    project: './tsconfig.json',
    extraFileExtensions: ['vue','.vue']
  },
  settings: {
    'import/resolver': {
      webpack: {
        config: require.resolve('@vue/cli-service/webpack.config.js'),
      },
    },
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'quotes': ['error', 'single'],
    'semi': ['error', 'never'],
    'no-underscore-dangle': 0,
    'class-methods-use-this': 0,
    'unicorn/no-null': 0,
    'no-param-reassign':0,
    'unicorn/no-reduce':0,
    "global-require": 0,
    'unicorn/no-abusive-eslint-disable': 0,
    '@typescript-eslint/ban-ts-ignore': 0,
    'import/no-cycle':0,
    'unicorn/filename-case': [
      'error',
      {
        'case': 'kebabCase',
        'ignore': [
          '^[A-Za-z]+\\.vue$'
        ]
      }
    ],
    '@typescript-eslint/lines-between-class-members': [
      'error',
      {
        'exceptAfterSingleLine': true
      }
    ],
    'curly': ['warn', 'all'],
    'vue/no-parsing-error': [
      2,
      {
        'x-invalid-end-tag': false
      }
    ],
    'import/order': 'off'
  },
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)'
      ],
      env: {
        jest: true
      }
    }
  ]
}
