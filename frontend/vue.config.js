/* eslint-disable */
const StyleLintPlugin = require('stylelint-webpack-plugin')

module.exports = {
  publicPath: '/dlt',
  lintOnSave: process.env.NODE_ENV === 'production' ? false : 'default',
  productionSourceMap: false,
  configureWebpack: (config) => {
    const plugins = [
      new StyleLintPlugin({
        files: ['src/**/*.{vue,less}'],
        fix: true,
      }),
    ]
    config.optimization = {
      splitChunks: {
        cacheGroups: {
          vendors: {
            name: 'chunk-vendors',
            test: /[\/]node_modules[\/]/,
            priority: -10,
            chunks: 'initial',
          },
          common: {
            name: 'chunk-common',
            minChunks: 2,
            priority: -20,
            chunks: 'initial',
            reuseExistingChunk: true,
          },
          visChartsPlus: {
            name: 'chunk-vischarts-plus',
            test: /[\/]node_modules[\/]@zjlabvis[\/]vis-charts-plus[\/]/,
            chunks: 'all',
            priority: -5,
            reuseExistingChunk: true,
          },
          antv: {
            name: 'antv',
            test: /[\/]node_modules[\/]@antv[\/]/,
            chunks: 'all',
            priority: -6,
            reuseExistingChunk: true,
          },
        },
      },
    }
    return { plugins }
  },
  chainWebpack: (config) => {
    config
      .plugin('html')
      .tap(args => {
        args[0].title = '在线血缘解析'
        return args
      })
  },
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          modifyVars: {
            'primary-color': '#5561ff',
            'link-color': '#5561ff',
            'border-radius-base': '2px',
          },
          javascriptEnabled: true,
        },
      },
    },
  },
  devServer: {
    proxy: {
      '/api': {
        target: 'http://10.5.24.17:7777',
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/dlt/api/(.*)': '/$1',
        },
      },
    },
  },
}
