<div align="center"><a href="https://git.zjvis.org/nebula-platform/nebula" target="_blank" rel="noopener noreferrer"><img width="120px" src="https://git.zjvis.org/nebula-platform/nebula/raw/dev/docs/logo.png" alt="Logo"></a></a></a></div>

<div align="center"><h1>在线血缘解析</h1></div>

## ⌨️ Development

You will need [Node.js](http://nodejs.org) **version 10+**.

After cloning the repo, run:

``` bash
$ npm install # install the dependencies of the project
```

Common NPM scripts:

``` bash
# watch and auto re-build the project
$ npm run serve

# run unit tests
$ npm run test:unit

# compile and minify for production
$ npm run build

# lint and fix files
$ npm run lint
```

## License

[MIT](http://opensource.org/licenses/MIT)

Copyright (c) 2020, Zhejiang Lab
