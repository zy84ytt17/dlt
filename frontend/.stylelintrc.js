module.exports = {
    extends: [
        'stylelint-config-sass-guidelines'
    ],
    rules: {
        'scss/dollar-variable-pattern': '[A-Z0-9_]+',
        'max-nesting-depth': 10,
        "selector-max-compound-selectors": 10,
        'selector-class-pattern': null,
        'declaration-block-semicolon-newline-after':null,
        'function-parentheses-space-inside':null,
        'indentation':null

    }
}
