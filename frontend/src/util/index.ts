import gradientColor from '@/util/gradient-color'
import checkEditName from '@/util/check-util'
import arrayToObjectByKey from '@/util/array-to-object'

export {
  gradientColor, // 颜色渐变
  checkEditName,
  arrayToObjectByKey,
}
