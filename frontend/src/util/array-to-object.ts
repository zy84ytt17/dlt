function arrayToObjectByKey(list: any[], key: string, valueKey: string) {
  const result: any = {}
  list.forEach((item: any) => {
    if (result[item[key]]) {
      result[item[key]].push(item[valueKey])
    } else {
      result[item[key]] = [item[valueKey]]
    }
  })
  return result
}

export default arrayToObjectByKey
