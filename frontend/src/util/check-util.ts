/**
 * check 编辑的名称是否为空， 全为空格， 去除首尾空格后与原名相同， 与其他名称相同
 * @param current 编辑的当前项
 * @param checkArray 所有项
 * @param key 唯一键
 * @param nameKey name 键
 * @param newName 新名称
 * @return flag 'pass'-正常， 'space'-全空字符串， 'repetition'-重名， 'no-change'-和原名相同， 'empty'-空
 */
export default function checkEditName(
  current: any,
  checkArray: any[],
  key: string,
  nameKey: string,
  newName: string
) {
  let flag: string = 'pass'
  if (newName.length > 0) {
    if (newName.match(/^ *$/)) {
      flag = 'space'
    } else if (current[nameKey] === newName.trim()) {
      // 和原名相同
      flag = 'no-change'
    } else {
      // 检查重名
      flag = checkArray.find(
        (item: any) => newName === item[nameKey] && item[key] !== current[key]
      )
        ? 'repetition'
        : 'pass'
    }
  } else {
    flag = 'empty'
  }
  return flag
}
