/**
 * 颜色工具
 * HexToRgb 转换十六进制颜色为RGB颜色
 * RgbToHex 转换RGB颜色为十六进制颜色
 * getDarkColor 获取深色
 * getLightColor 获取浅色
 */

/**
 * hex颜色转rgb颜色
 */
export function HexToRgb(color: string) {
  color = color.replace('#', '') // replace替换查找的到的字符串
  const hxs: any = color.match(/../g) // match得到查询数组
  for (let i = 0; i < 3; i += 1) {
    hxs[i] = Number.parseInt(hxs[i], 16)
  }
  return hxs
}

/**
 * GRB颜色转Hex颜色
 * @param a
 * @param b
 * @param c
 * @constructor
 */
export function RgbToHex(a: number, b: number, c: number) {
  const hexes = [a.toString(16), b.toString(16), c.toString(16)]
  for (let i = 0; i < 3; i += 1) {
    if (hexes[i].length === 1) {
      hexes[i] = `0${hexes[i]}`
    }
  }
  return `#${hexes.join('')}`
}

/**
 * 得到hex颜色值为color的加深颜色值，level为加深的程度，限0-1之间
 * @param color hex颜色值
 * @param level 加深的程度
 */
export function getDarkColor(color: string, level: number) {
  const rgba = HexToRgb(color)
  for (let i = 0; i < 3; i += 1) {
    rgba[i] = Math.floor(rgba[i] * (1 - level))
  } // floor 向下取整
  return RgbToHex(rgba[0], rgba[1], rgba[2])
}

/**
 * 得到hex颜色值为color的减淡颜色值，level为减浅的程度，限0-1之间
 * @param color hex颜色值
 * @param level 减浅的程度
 * @param type 减淡类型，1 | 2
 */
export function getLightColor(color: string, level: number, type: 1 | 2 = 1) {
  const rgba = HexToRgb(color)
  const typeLeave: number = type === 1 ? level : 1 - level
  for (let i = 0; i < 3; i += 1) {
    rgba[i] = Math.floor((255 - rgba[i]) * typeLeave + rgba[i])
  }
  return RgbToHex(rgba[0], rgba[1], rgba[2])
}
