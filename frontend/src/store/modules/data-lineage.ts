/**
 * data lineage store
 */
import {
  Module,
  Mutation,
  VuexModule,
  getModule,
  Action,
} from 'vuex-module-decorators'
import store from '@/store'
import { parseAllLineageApi } from '@/api/data-lineage'

@Module({ dynamic: true, namespaced: true, name: 'DataLineage', store })
class DataLineage extends VuexModule {
  private _loading: boolean = false
  private _loadingTip: string = 'loading...'
  private _tableDataLineage: any = null // 表格级血缘数据
  private _fieldDataLineage: any = null // 字段级血缘数据

  public get tableDataLineage() {
    return this._tableDataLineage
  }
  public get fieldDataLineage() {
    return this._fieldDataLineage
  }
  public get loading() {
    return this._loading
  }
  public get loadingTip() {
    return this._loadingTip
  }

  @Mutation
  public setTableDataLineage(data: any) {
    this._tableDataLineage = data
  }
  @Mutation
  public setFieldDataLineage(data: any) {
    this._fieldDataLineage = data
  }

  @Mutation
  public setLoading(value: boolean) {
    this._loading = value
  }

  @Action({ commit: 'setDataLineage' })
  public async register(passData: any) {
    const response = await parseAllLineageApi({
      data: passData,
      params: { code: passData.phoneCode },
    })
    return response.data.result
  }
}

export default getModule(DataLineage)
