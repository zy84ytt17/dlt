export type DataSourceType =
  | 'MYSQL'
  | 'POSTGRES'
  | 'ORACLE'
  | 'SQLSERVER'
  | 'CLICKHOUSE'

export interface DataSourceTypeObject {
  label: string
  value: DataSourceType
  icon: string
}
