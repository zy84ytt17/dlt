import { DataSourceTypeObject } from '@/components/home/interface'

/**
 * 数据源类型 options
 */
const dataSourceTypeOptions: Array<DataSourceTypeObject> = [
  {
    label: 'ClickHouse',
    value: 'CLICKHOUSE',
    icon: 'icon-ClickHouse',
  },
  {
    label: 'MySQL',
    value: 'MYSQL',
    icon: 'icon-MySQL',
  },
  {
    label: 'PostgreSQL',
    value: 'POSTGRES',
    icon: 'icon-PostgreSQL',
  },
  {
    label: 'Oracle',
    value: 'ORACLE',
    icon: 'icon-Oracle',
  },
  {
    label: 'SQL Server',
    value: 'SQLSERVER',
    icon: 'icon-a-MicrosoftSQLServer',
  },
]

export default dataSourceTypeOptions
