/**
 * codemirror 重写 sql mode 扩展 ClickHouse
 * 包含模式:
 *  text/x-sql: 通用SQL模式。它不是一个标准，它只是试图支持普遍支持的东西
 *  text/x-clickhouse: ClickHouse
 *  text/x-mysql: MySQL
 *  text/x-pgsql: PostgreSQL
 */
import CodeMirror from 'codemirror'

CodeMirror.defineMode('sql', function (config, parserConfig) {
  var client = parserConfig.client || {},
    atoms = parserConfig.atoms || { false: true, true: true, null: true },
    builtin = parserConfig.builtin || set(defaultBuiltin),
    keywords = parserConfig.keywords || set(sqlKeywords),
    operatorChars = parserConfig.operatorChars || /^[*+\-%<>!=&|~^\/]/,
    support = parserConfig.support || {},
    hooks = parserConfig.hooks || {},
    dateSQL = parserConfig.dateSQL || {
      date: true,
      time: true,
      timestamp: true,
    },
    backslashStringEscapes = parserConfig.backslashStringEscapes !== false,
    brackets = parserConfig.brackets || /^[\{}\(\)\[\]]/,
    punctuation = parserConfig.punctuation || /^[;.,:]/

  function tokenBase(stream, state) {
    var ch = stream.next()

    // call hooks from the mime type
    if (hooks[ch]) {
      var result = hooks[ch](stream, state)
      if (result !== false) return result
    }

    if (
      support.hexNumber &&
      ((ch == '0' && stream.match(/^[xX][0-9a-fA-F]+/)) ||
        ((ch == 'x' || ch == 'X') && stream.match(/^'[0-9a-fA-F]*'/)))
    ) {
      // hex
      // ref: https://dev.mysql.com/doc/refman/8.0/en/hexadecimal-literals.html
      return 'number'
    } else if (
      support.binaryNumber &&
      (((ch == 'b' || ch == 'B') && stream.match(/^'[01]*'/)) ||
        (ch == '0' && stream.match(/^b[01]+/)))
    ) {
      // bitstring
      // ref: https://dev.mysql.com/doc/refman/8.0/en/bit-value-literals.html
      return 'number'
    } else if (ch.charCodeAt(0) > 47 && ch.charCodeAt(0) < 58) {
      // numbers
      // ref: https://dev.mysql.com/doc/refman/8.0/en/number-literals.html
      stream.match(/^[0-9]*(\.[0-9]+)?([eE][-+]?[0-9]+)?/)
      support.decimallessFloat && stream.match(/^\.(?!\.)/)
      return 'number'
    } else if (
      ch == '?' &&
      (stream.eatSpace() || stream.eol() || stream.eat(';'))
    ) {
      // placeholders
      return 'variable-3'
    } else if (ch == "'" || (ch == '"' && support.doubleQuote)) {
      // strings
      // ref: https://dev.mysql.com/doc/refman/8.0/en/string-literals.html
      state.tokenize = tokenLiteral(ch)
      return state.tokenize(stream, state)
    } else if (
      ((support.nCharCast && (ch == 'n' || ch == 'N')) ||
        (support.charsetCast &&
          ch == '_' &&
          stream.match(/[a-z][a-z0-9]*/i))) &&
      (stream.peek() == "'" || stream.peek() == '"')
    ) {
      // charset casting: _utf8'str', N'str', n'str'
      // ref: https://dev.mysql.com/doc/refman/8.0/en/string-literals.html
      return 'keyword'
    } else if (
      support.escapeConstant &&
      (ch == 'e' || ch == 'E') &&
      (stream.peek() == "'" || (stream.peek() == '"' && support.doubleQuote))
    ) {
      // escape constant: E'str', e'str'
      // ref: https://www.postgresql.org/docs/current/sql-syntax-lexical.html#SQL-SYNTAX-STRINGS-ESCAPE
      state.tokenize = function (stream, state) {
        return (state.tokenize = tokenLiteral(stream.next(), true))(
          stream,
          state
        )
      }
      return 'keyword'
    } else if (support.commentSlashSlash && ch == '/' && stream.eat('/')) {
      // 1-line comment
      stream.skipToEnd()
      return 'comment'
    } else if (
      (support.commentHash && ch == '#') ||
      (ch == '-' &&
        stream.eat('-') &&
        (!support.commentSpaceRequired || stream.eat(' ')))
    ) {
      // 1-line comments
      // ref: https://kb.askmonty.org/en/comment-syntax/
      stream.skipToEnd()
      return 'comment'
    } else if (ch == '/' && stream.eat('*')) {
      // multi-line comments
      // ref: https://kb.askmonty.org/en/comment-syntax/
      state.tokenize = tokenComment(1)
      return state.tokenize(stream, state)
    } else if (ch == '.') {
      // .1 for 0.1
      if (support.zerolessFloat && stream.match(/^(?:\d+(?:e[+-]?\d+)?)/i))
        return 'number'
      if (stream.match(/^\.+/)) return null
      if (stream.match(/^[\w\d_$#]+/)) return 'variable-2'
    } else if (operatorChars.test(ch)) {
      // operators
      stream.eatWhile(operatorChars)
      return 'operator'
    } else if (brackets.test(ch)) {
      // brackets
      return 'bracket'
    } else if (punctuation.test(ch)) {
      // punctuation
      stream.eatWhile(punctuation)
      return 'punctuation'
    } else if (
      ch == '{' &&
      (stream.match(/^( )*(d|D|t|T|ts|TS)( )*'[^']*'( )*}/) ||
        stream.match(/^( )*(d|D|t|T|ts|TS)( )*"[^"]*"( )*}/))
    ) {
      // dates (weird ODBC syntax)
      // ref: https://dev.mysql.com/doc/refman/8.0/en/date-and-time-literals.html
      return 'number'
    } else {
      stream.eatWhile(/^[_\w\d]/)
      var word = stream.current().toLowerCase()
      // dates (standard SQL syntax)
      // ref: https://dev.mysql.com/doc/refman/8.0/en/date-and-time-literals.html
      if (
        dateSQL.hasOwnProperty(word) &&
        (stream.match(/^( )+'[^']*'/) || stream.match(/^( )+"[^"]*"/))
      )
        return 'number'
      if (atoms.hasOwnProperty(word)) return 'atom'
      if (builtin.hasOwnProperty(word)) return 'type'
      if (keywords.hasOwnProperty(word)) return 'keyword'
      if (client.hasOwnProperty(word)) return 'builtin'
      return null
    }
  }

  // 'string', with char specified in quote escaped by '\'
  function tokenLiteral(quote, backslashEscapes) {
    return function (stream, state) {
      var escaped = false,
        ch
      while ((ch = stream.next()) != null) {
        if (ch == quote && !escaped) {
          state.tokenize = tokenBase
          break
        }
        escaped =
          (backslashStringEscapes || backslashEscapes) && !escaped && ch == '\\'
      }
      return 'string'
    }
  }
  function tokenComment(depth) {
    return function (stream, state) {
      var m = stream.match(/^.*?(\/\*|\*\/)/)
      if (!m) stream.skipToEnd()
      else if (m[1] == '/*') state.tokenize = tokenComment(depth + 1)
      else if (depth > 1) state.tokenize = tokenComment(depth - 1)
      else state.tokenize = tokenBase
      return 'comment'
    }
  }

  function pushContext(stream, state, type) {
    state.context = {
      prev: state.context,
      indent: stream.indentation(),
      col: stream.column(),
      type: type,
    }
  }

  function popContext(state) {
    state.indent = state.context.indent
    state.context = state.context.prev
  }

  return {
    startState: function () {
      return { tokenize: tokenBase, context: null }
    },

    token: function (stream, state) {
      if (stream.sol()) {
        if (state.context && state.context.align == null)
          state.context.align = false
      }
      if (state.tokenize == tokenBase && stream.eatSpace()) return null

      var style = state.tokenize(stream, state)
      if (style == 'comment') return style

      if (state.context && state.context.align == null)
        state.context.align = true

      var tok = stream.current()
      if (tok == '(') pushContext(stream, state, ')')
      else if (tok == '[') pushContext(stream, state, ']')
      else if (state.context && state.context.type == tok) popContext(state)
      return style
    },

    indent: function (state, textAfter) {
      var cx = state.context
      if (!cx) return CodeMirror.Pass
      var closing = textAfter.charAt(0) == cx.type
      if (cx.align) return cx.col + (closing ? 0 : 1)
      else return cx.indent + (closing ? 0 : config.indentUnit)
    },

    blockCommentStart: '/*',
    blockCommentEnd: '*/',
    lineComment: support.commentSlashSlash
      ? '//'
      : support.commentHash
      ? '#'
      : '--',
    closeBrackets: '()[]{}\'\'""``',
    config: parserConfig,
  }
})

// `identifier`
function hookIdentifier(stream) {
  // MySQL/MariaDB identifiers
  // ref: https://dev.mysql.com/doc/refman/8.0/en/identifier-qualifiers.html
  var ch
  while ((ch = stream.next()) != null) {
    if (ch == '`' && !stream.eat('`')) return 'variable-2'
  }
  stream.backUp(stream.current().length - 1)
  return stream.eatWhile(/\w/) ? 'variable-2' : null
}

// variable token
function hookVar(stream) {
  // variables
  // @@prefix.varName @varName
  // varName can be quoted with ` or ' or "
  // ref: https://dev.mysql.com/doc/refman/8.0/en/user-variables.html
  if (stream.eat('@')) {
    stream.match('session.')
    stream.match('local.')
    stream.match('global.')
  }

  if (stream.eat("'")) {
    stream.match(/^.*'/)
    return 'variable-2'
  } else if (stream.eat('"')) {
    stream.match(/^.*"/)
    return 'variable-2'
  } else if (stream.eat('`')) {
    stream.match(/^.*`/)
    return 'variable-2'
  } else if (stream.match(/^[0-9a-zA-Z$\.\_]+/)) {
    return 'variable-2'
  }
  return null
}

// short client keyword token
function hookClient(stream) {
  // \N means NULL
  // ref: https://dev.mysql.com/doc/refman/8.0/en/null-values.html
  if (stream.eat('N')) {
    return 'atom'
  }
  // \g, etc
  // ref: https://dev.mysql.com/doc/refman/8.0/en/mysql-commands.html
  return stream.match(/^[a-zA-Z.#!?]/) ? 'variable-2' : null
}

// turn a space-separated list into an array
// 将空格分隔的列表转换为数组
function set(str) {
  var obj = {},
    words = str.split(' ')
  for (var i = 0; i < words.length; ++i) obj[words[i]] = true
  return obj
}

// these keywords are used by all SQL dialects (however, a mode can still overwrite it)
// 所有SQL方言都可以使用这些关键字(但是，模式仍然可以覆盖它)。
var sqlKeywords =
  'alter and as asc between by count create delete desc distinct drop from group having in insert into is join like not on or order select set table union update values where limit '
var defaultBuiltin =
  'bool boolean bit blob enum long longblob longtext medium mediumblob mediumint mediumtext time timestamp tinyblob tinyint tinytext text bigint int int1 int2 int3 int4 int8 integer float float4 float8 double char varbinary varchar varcharacter precision real date datetime year unsigned signed decimal numeric'
var clickHouseKeywords = 'global cluster '

// 通用SQL模式。它不是一个标准，它只是试图支持普遍支持的东西
CodeMirror.defineMIME('text/x-sql', {
  name: 'sql',
  keywords: set(sqlKeywords + 'begin'),
  builtin: set(defaultBuiltin),
  atoms: set('false true null unknown'),
  dateSQL: set('date time timestamp'),
  support: set('doubleQuote binaryNumber hexNumber'),
})

// ClickHouse
CodeMirror.defineMIME('text/x-clickhouse', {
  name: 'sql',
  keywords: set(sqlKeywords + clickHouseKeywords + 'begin'),
  builtin: set(defaultBuiltin),
  atoms: set('false true null unknown'),
  dateSQL: set('date time timestamp'),
  support: set('doubleQuote binaryNumber hexNumber'),
})

// MySQL
CodeMirror.defineMIME('text/x-mysql', {
  name: 'sql',
  client: set(
    'charset clear connect edit ego exit go help nopager notee nowarning pager print prompt quit rehash source status system tee'
  ),
  keywords: set(
    sqlKeywords +
      'accessible action add after algorithm all analyze asensitive at authors auto_increment autocommit avg avg_row_length before binary binlog both btree cache call cascade cascaded case catalog_name chain change changed character check checkpoint checksum class_origin client_statistics close coalesce code collate collation collations column columns comment commit committed completion concurrent condition connection consistent constraint contains continue contributors convert cross current current_date current_time current_timestamp current_user cursor data database databases day_hour day_microsecond day_minute day_second deallocate dec declare default delay_key_write delayed delimiter des_key_file describe deterministic dev_pop dev_samp deviance diagnostics directory disable discard distinctrow div dual dumpfile each elseif enable enclosed end ends engine engines enum errors escape escaped even event events every execute exists exit explain extended fast fetch field fields first flush for force foreign found_rows full fulltext function general get global grant grants group group_concat handler hash help high_priority hosts hour_microsecond hour_minute hour_second if ignore ignore_server_ids import index index_statistics infile inner innodb inout insensitive insert_method install interval invoker isolation iterate key keys kill language last leading leave left level limit linear lines list load local localtime localtimestamp lock logs low_priority master master_heartbeat_period master_ssl_verify_server_cert masters match max max_rows maxvalue message_text middleint migrate min min_rows minute_microsecond minute_second mod mode modifies modify mutex mysql_errno natural next no no_write_to_binlog offline offset one online open optimize option optionally out outer outfile pack_keys parser partition partitions password phase plugin plugins prepare preserve prev primary privileges procedure processlist profile profiles purge query quick range read read_write reads real rebuild recover references regexp relaylog release remove rename reorganize repair repeatable replace require resignal restrict resume return returns revoke right rlike rollback rollup row row_format rtree savepoint schedule schema schema_name schemas second_microsecond security sensitive separator serializable server session share show signal slave slow smallint snapshot soname spatial specific sql sql_big_result sql_buffer_result sql_cache sql_calc_found_rows sql_no_cache sql_small_result sqlexception sqlstate sqlwarning ssl start starting starts status std stddev stddev_pop stddev_samp storage straight_join subclass_origin sum suspend table_name table_statistics tables tablespace temporary terminated to trailing transaction trigger triggers truncate uncommitted undo uninstall unique unlock upgrade usage use use_frm user user_resources user_statistics using utc_date utc_time utc_timestamp value variables varying view views warnings when while with work write xa xor year_month zerofill begin do then else loop repeat'
  ),
  builtin: set(
    'bool boolean bit blob decimal double float long longblob longtext medium mediumblob mediumint mediumtext time timestamp tinyblob tinyint tinytext text bigint int int1 int2 int3 int4 int8 integer float float4 float8 double char varbinary varchar varcharacter precision date datetime year unsigned signed numeric'
  ),
  atoms: set('false true null unknown'),
  operatorChars: /^[*+\-%<>!=&|^]/,
  dateSQL: set('date time timestamp'),
  support: set(
    'decimallessFloat zerolessFloat binaryNumber hexNumber doubleQuote nCharCast charsetCast commentHash commentSpaceRequired'
  ),
  hooks: {
    '@': hookVar,
    '`': hookIdentifier,
    '\\': hookClient,
  },
})

// PostgreSQL
CodeMirror.defineMIME('text/x-pgsql', {
  name: 'sql',
  client: set('source'),
  keywords: set(
    sqlKeywords +
      'a abort abs absent absolute access according action ada add admin after aggregate alias all allocate also alter always analyse analyze and any are array array_agg array_max_cardinality as asc asensitive assert assertion assignment asymmetric at atomic attach attribute attributes authorization avg backward base64 before begin begin_frame begin_partition bernoulli between bigint binary bit bit_length blob blocked bom boolean both breadth by c cache call called cardinality cascade cascaded case cast catalog catalog_name ceil ceiling chain char char_length character character_length character_set_catalog character_set_name character_set_schema characteristics characters check checkpoint class class_origin clob close cluster coalesce cobol collate collation collation_catalog collation_name collation_schema collect column column_name columns command_function command_function_code comment comments commit committed concurrently condition condition_number configuration conflict connect connection connection_name constant constraint constraint_catalog constraint_name constraint_schema constraints constructor contains content continue control conversion convert copy corr corresponding cost count covar_pop covar_samp create cross csv cube cume_dist current current_catalog current_date current_default_transform_group current_path current_role current_row current_schema current_time current_timestamp current_transform_group_for_type current_user cursor cursor_name cycle data database datalink datatype date datetime_interval_code datetime_interval_precision day db deallocate debug dec decimal declare default defaults deferrable deferred defined definer degree delete delimiter delimiters dense_rank depends depth deref derived desc describe descriptor detach detail deterministic diagnostics dictionary disable discard disconnect dispatch distinct dlnewcopy dlpreviouscopy dlurlcomplete dlurlcompleteonly dlurlcompletewrite dlurlpath dlurlpathonly dlurlpathwrite dlurlscheme dlurlserver dlvalue do document domain double drop dump dynamic dynamic_function dynamic_function_code each element else elseif elsif empty enable encoding encrypted end end_frame end_partition endexec enforced enum equals errcode error escape event every except exception exclude excluding exclusive exec execute exists exit exp explain expression extension external extract false family fetch file filter final first first_value flag float floor following for force foreach foreign fortran forward found frame_row free freeze from fs full function functions fusion g general generated get global go goto grant granted greatest group grouping groups handler having header hex hierarchy hint hold hour id identity if ignore ilike immediate immediately immutable implementation implicit import in include including increment indent index indexes indicator info inherit inherits initially inline inner inout input insensitive insert instance instantiable instead int integer integrity intersect intersection interval into invoker is isnull isolation join k key key_member key_type label lag language large last last_value lateral lead leading leakproof least left length level library like like_regex limit link listen ln load local localtime localtimestamp location locator lock locked log logged loop lower m map mapping match matched materialized max max_cardinality maxvalue member merge message message_length message_octet_length message_text method min minute minvalue mod mode modifies module month more move multiset mumps name names namespace national natural nchar nclob nesting new next nfc nfd nfkc nfkd nil no none normalize normalized not nothing notice notify notnull nowait nth_value ntile null nullable nullif nulls number numeric object occurrences_regex octet_length octets of off offset oids old on only open operator option options or order ordering ordinality others out outer output over overlaps overlay overriding owned owner p pad parallel parameter parameter_mode parameter_name parameter_ordinal_position parameter_specific_catalog parameter_specific_name parameter_specific_schema parser partial partition pascal passing passthrough password path percent percent_rank percentile_cont percentile_disc perform period permission pg_context pg_datatype_name pg_exception_context pg_exception_detail pg_exception_hint placing plans pli policy portion position position_regex power precedes preceding precision prepare prepared preserve primary print_strict_params prior privileges procedural procedure procedures program public publication query quote raise range rank read reads real reassign recheck recovery recursive ref references referencing refresh regr_avgx regr_avgy regr_count regr_intercept regr_r2 regr_slope regr_sxx regr_sxy regr_syy reindex relative release rename repeatable replace replica requiring reset respect restart restore restrict result result_oid return returned_cardinality returned_length returned_octet_length returned_sqlstate returning returns reverse revoke right role rollback rollup routine routine_catalog routine_name routine_schema routines row row_count row_number rows rowtype rule savepoint scale schema schema_name schemas scope scope_catalog scope_name scope_schema scroll search second section security select selective self sensitive sequence sequences serializable server server_name session session_user set setof sets share show similar simple size skip slice smallint snapshot some source space specific specific_name specifictype sql sqlcode sqlerror sqlexception sqlstate sqlwarning sqrt stable stacked standalone start state statement static statistics stddev_pop stddev_samp stdin stdout storage strict strip structure style subclass_origin submultiset subscription substring substring_regex succeeds sum symmetric sysid system system_time system_user t table table_name tables tablesample tablespace temp template temporary text then ties time timestamp timezone_hour timezone_minute to token top_level_count trailing transaction transaction_active transactions_committed transactions_rolled_back transform transforms translate translate_regex translation treat trigger trigger_catalog trigger_name trigger_schema trim trim_array true truncate trusted type types uescape unbounded uncommitted under unencrypted union unique unknown unlink unlisten unlogged unnamed unnest until untyped update upper uri usage use_column use_variable user user_defined_type_catalog user_defined_type_code user_defined_type_name user_defined_type_schema using vacuum valid validate validator value value_of values var_pop var_samp varbinary varchar variable_conflict variadic varying verbose version versioning view views volatile warning when whenever where while whitespace width_bucket window with within without work wrapper write xml xmlagg xmlattributes xmlbinary xmlcast xmlcomment xmlconcat xmldeclaration xmldocument xmlelement xmlexists xmlforest xmliterate xmlnamespaces xmlparse xmlpi xmlquery xmlroot xmlschema xmlserialize xmltable xmltext xmlvalidate year yes zone'
  ),
  builtin: set(
    'bigint int8 bigserial serial8 bit varying varbit boolean bool box bytea character char varchar cidr circle date double precision float8 inet integer int int4 interval json jsonb line lseg macaddr macaddr8 money numeric decimal path pg_lsn point polygon real float4 smallint int2 smallserial serial2 serial serial4 text time zone timetz timestamp timestamptz tsquery tsvector txid_snapshot uuid xml'
  ),
  atoms: set('false true null unknown'),
  operatorChars: /^[*\/+\-%<>!=&|^\/#@?~]/,
  backslashStringEscapes: false,
  dateSQL: set('date time timestamp'),
  support: set(
    'decimallessFloat zerolessFloat binaryNumber hexNumber nCharCast charsetCast escapeConstant'
  ),
})

/*
  How Properties of Mime Types are used by SQL Mode
  SQL模式如何使用Mime类型的属性
  =================================================

  keywords:
    A list of keywords you want to be highlighted.
    要突出显示的关键字列表
  builtin:
    A list of builtin types you want to be highlighted (if you want types to be of class "builtin" instead of "keyword").
    你想要突出显示的内置类型列表(如果你想要类型是“builtin”类而不是“keyword”类)。
  operatorChars:
    All characters that must be handled as operators.
    必须作为操作符处理的所有字符。
  client:
    Commands parsed and executed by the client (not the server).
    由客户机(而不是服务器)解析和执行的命令。
  support:
    A list of supported syntaxes which are not common, but are supported by more than 1 DBMS.
    支持的语法列表，这些语法不常见，但被多个DBMS支持。
    * zerolessFloat: .1
    * decimallessFloat: 1.
    * hexNumber: X'01AF' X'01af' x'01AF' x'01af' 0x01AF 0x01af
    * binaryNumber: b'01' B'01' 0b01
    * doubleQuote: "string"
    * escapeConstant: E''
    * nCharCast: N'string'
    * charsetCast: _utf8'string'
    * commentHash: use # char for comments
    * commentSlashSlash: use // for comments
    * commentSpaceRequired: require a space after -- for comments
  atoms:
    Keywords that must be highlighted as atoms,. Some DBMS's support more atoms than others:
    必须突出显示为原子的关键字，。一些DBMS比其他DBMS支持更多的原子:
    UNKNOWN, INFINITY, UNDERFLOW, NaN...
  dateSQL:
    Used for date/time SQL standard syntax, because not all DBMS's support same temporal types.
    用于日期/时间SQL标准语法，因为并非所有DBMS都支持相同的时态类型。
*/
