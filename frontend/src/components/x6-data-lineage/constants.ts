/**
 * constant
 */
import { KV } from '@/interface/interface'
import { TableTypeInfo } from '@/components/x6-data-lineage/interface'

export const NODE_HEADER_HEIGHT: number = 24 // 节点 header 高度
export const NODE_LINE_HEIGHT: number = 24 // 节点内 表名 / 字段项 高度
export const NODE_WIDTH: number = 180 // 节点宽度
export const EDGE_DEFAULT_COLOR: string = '#D9D9D9' // 边的默认色
export const EDGE_LINK_HIGHLIGHT_COLOR: string = '#2A4EDB' // 边链路高亮色
export const EDGE_WARNING_HIGHLIGHT_COLOR: string = '#FF4D4F' // 边链路警告高亮色
export const GRAPH_SCALING_MIN: number = 0.2 // 画布可以缩放的最小级别
export const GRAPH_SCALING_MAX: number = 3 // 画布可以缩放的最大级别

// 表类型信息
export const TABLE_TYPE_INFO_MAP: KV<TableTypeInfo> = {
  DEFAULT: {
    databaseIcon: 'icon-shujuku',
    icon: 'icon-wulibiao',
    color: '#fff',
    theme: 'rgba(83,123,224,0.9)',
    rgb: '83,123,224',
    iconShadow: '#0A37AA',
    selectedBorderColor: '#2A4EDB',
  },
  // 更多表类型...
}
