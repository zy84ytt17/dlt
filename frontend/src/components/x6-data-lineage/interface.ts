export interface KeyValue<T> {
  [key: string]: T | any
}

export interface TableTypeInfo {
  databaseIcon: string
  icon: string
  color: string
  theme: string
  rgb: string
  iconShadow: string
  selectedBorderColor: string
}

export interface Tool {
  name?: string
  icon: string
  key: 'ZOOM_IN' | 'ZOOM_OUT' | 'FIT_VIEW' | 'FIT_CENTER' | 'INIT'
  disable?: any
}

// 字段级血缘 表字段详情
export interface TableFieldDetail {
  fieldName: string
  haveEntrance: boolean
  haveExit: boolean
}

// X6 Graph LineageGraph 选中/hover的节点
export interface NodeInfo {
  nodeId: string
  fieldName?: string
}

// X6 Graph LineageGraph 搜索查询到的节点
interface SearchNodes {
  id: string
  fieldName?: string
}

// X6 Graph LineageGraph 搜索查询到的边
type SearchEdges = string

// X6 Graph LineageGraph 搜索查询到的节点和边
export interface SearchResult {
  linkNodes: Array<SearchNodes>
  linkEdges: Array<SearchEdges>
}
