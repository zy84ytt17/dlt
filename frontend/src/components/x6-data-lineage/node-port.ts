/**
 * 节点连接桩
 */

import { NODE_HEADER_HEIGHT, NODE_LINE_HEIGHT, NODE_WIDTH } from './constants'

/**
 * 有出入度的链接桩
 */
export const haveEntranceExitNodePortAttributes: any = {
  border: {
    magnet: false, // 阻止拉出连线
    stroke: 'rgba(0,0,0,0.2)',
    fill: 'transparent',
    strokeWidth: 1,
    d: 'M -4 -5 H 4 V 5 H -4 Z',
    cursor: 'pointer',
    zIndex: 1,
  },
  center: {
    magnet: false,
    stroke: 'rgba(0,0,0,0.7)',
    strokeWidth: 1,
    d: 'M -2 0 H 2 M 0 -2 V 2',
    cursor: 'pointer',
    zIndex: 1,
  },
}

/**
 * 无出入度的链接桩
 */
const noEntranceExitNodePortAttributes: any = {
  border: {
    magnet: false,
    d: 'M 0 0 L 0 0',
  },
  center: {
    magnet: false,
    d: 'M 0 0 L 0 0',
  },
}

/**
 * 链接桩 svg
 */
const nodePortAttributes: any = {
  markup: [
    {
      tagName: 'path',
      selector: 'border',
    },
    {
      tagName: 'path',
      selector: 'center',
    },
  ],
}

/**
 * 节点链接桩分组
 */
export const nodePortsGroups: any = {
  in: {
    position: {
      name: 'absolute',
    },
    ...nodePortAttributes,
  },
  out: {
    position: {
      name: 'absolute',
    },
    ...nodePortAttributes,
  },
}

/**
 * 生成字段级血缘节点链接桩
 * @param field
 * @param index
 */
export function generateFieldNodePortsByFieldName(field: any, index: number) {
  const {
    fieldName = field,
    // haveEntrance = false,
    // haveExit = false
  } = field
  const nodePorts: any[] = []
  nodePorts.push({
    group: 'in',
    id: `in-${fieldName}`,
    args: {
      x: 0,
      // TODO 全部不显示锚点
      // x: haveEntrance ? -4.5 : 0,
      y:
        NODE_HEADER_HEIGHT +
        (index + 0.5) * NODE_LINE_HEIGHT +
        NODE_LINE_HEIGHT, // 表名另起一行
    },
    // TODO 全部不显示锚点
    attrs: noEntranceExitNodePortAttributes,
    // attrs: {
    //   ...(haveEntrance
    //     ? haveEntranceExitNodePortAttributes
    //     : noEntranceExitNodePortAttributes),
    // },
  })
  nodePorts.push({
    group: 'out',
    id: `out-${fieldName}`,
    args: {
      x: NODE_WIDTH,
      // TODO 全部不显示锚点
      // x: haveExit ? NODE_WIDTH + 4.5 : NODE_WIDTH,
      y:
        NODE_HEADER_HEIGHT +
        (index + 0.5) * NODE_LINE_HEIGHT +
        NODE_LINE_HEIGHT, // 表名另起一行
    },
    // TODO 全部不显示锚点
    attrs: noEntranceExitNodePortAttributes,
    // attrs: {
    //   ...(haveExit
    //     ? haveEntranceExitNodePortAttributes
    //     : noEntranceExitNodePortAttributes),
    // },
  })
  // }
  return nodePorts
}

/**
 * 生成表格级血缘节点链接桩
 * @param haveEntrance 有上游节点
 * @param haveExit 有下游节点
 */
export function generateTableNodePorts(
  haveEntrance?: boolean,
  haveExit?: boolean
): any[] {
  const nodePorts: any[] = []
  nodePorts.push({
    group: 'in',
    id: 'in-port',
    args: {
      x: haveEntrance ? 0 : 0,
      // TODO 全部不显示锚点
      // x: haveEntrance ? -4.5 : 0,
      y: NODE_HEADER_HEIGHT + NODE_LINE_HEIGHT * 0.5,
    },
    // TODO 全部不显示锚点
    attrs: noEntranceExitNodePortAttributes,
    // attrs: {
    //   ...(haveEntrance
    //     ? haveEntranceExitNodePortAttributes
    //     : noEntranceExitNodePortAttributes),
    // },
  })
  nodePorts.push({
    group: 'out',
    id: 'out-port',
    args: {
      x: haveExit ? NODE_WIDTH : NODE_WIDTH,
      // TODO 全部不显示锚点
      // x: haveExit ? NODE_WIDTH + 4.5 : NODE_WIDTH,
      y: NODE_HEADER_HEIGHT + NODE_LINE_HEIGHT * 0.5,
    },
    // TODO 全部不显示锚点
    attrs: noEntranceExitNodePortAttributes,
    // attrs: {
    //   ...(haveExit
    //     ? haveEntranceExitNodePortAttributes
    //     : noEntranceExitNodePortAttributes),
    // },
  })
  return nodePorts
}

/**
 * 根据是否有出入度更新链接桩样式
 * @param node
 * @param portId
 * @param haveEntranceExit
 */
export function updateNodePortStyle(
  node: any,
  portId: string,
  haveEntranceExit: boolean = false
) {
  const [prefix] = portId.split('-')
  node.portProp(portId, {
    // TODO 全部不显示锚点
    attrs: noEntranceExitNodePortAttributes,
    // attrs: {
    //   ...(haveEntranceExit
    //     ? haveEntranceExitNodePortAttributes
    //     : noEntranceExitNodePortAttributes),
    // },
    args: {
      x: haveEntranceExit
        ? prefix === 'in'
          ? -4.5
          : NODE_WIDTH + 4.5
        : prefix === 'in'
        ? 0
        : NODE_WIDTH,
    },
  })
}

/**
 * 根据节点业务数据更新
 * @param node
 */
export function updateNodePortsStyle(node: any) {
  const { tableFieldDetailList, haveEntrance, haveExit } = node.data
  // 字段级
  if (tableFieldDetailList) {
    tableFieldDetailList.forEach((field: any) => {
      const { fieldName } = field
      updateNodePortStyle(node, `in-${fieldName}`, field.haveEntrance)
      updateNodePortStyle(node, `out-${fieldName}`, field.haveExit)
    })
  } else {
    updateNodePortStyle(node, 'in-port', haveEntrance)
    updateNodePortStyle(node, 'out-port', haveExit)
  }
}
