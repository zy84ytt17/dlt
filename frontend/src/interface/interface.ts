// KV
export interface KV<T> {
  [key: string]: T | any
}

// Option
export interface Option {
  label: string
  value: number | string
}
