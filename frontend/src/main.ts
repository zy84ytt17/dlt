import Vue from 'vue'
import {
  FormModel,
  Layout,
  Menu,
  Icon,
  Modal,
  Dropdown,
  Button,
  Tooltip,
  Input,
  Radio,
  Select,
  Checkbox,
  message,
  Tabs,
  Spin,
  Slider,
  Space,
  Collapse,
  Col,
  Row,
  Alert,
  ConfigProvider,
} from 'ant-design-vue'
import 'ant-design-vue/dist/antd.less'
// @ts-ignore
import App from './App.vue'
import router from './router'
import store from './store'
import { publicPath } from './api/constants'

Vue.use(FormModel)
Vue.use(Layout)
Vue.use(Layout.Header)
Vue.use(Layout.Sider)
Vue.use(Menu)
Vue.use(Menu.Item)
Vue.use(Icon)
Vue.use(Modal)
Vue.use(Dropdown)
Vue.use(Button)
Vue.use(Tooltip)
Vue.use(Tabs)
Vue.use(Input)
Vue.use(Radio.Group)
Vue.use(Radio)
Vue.use(Select)
Vue.use(Space)
Vue.use(Checkbox)
Vue.use(Checkbox.Group)
Vue.use(Spin)
Vue.use(Input.TextArea)
Vue.use(Slider)
Vue.use(Collapse)
Vue.use(Col)
Vue.use(Row)
Vue.use(Alert)
Vue.use(ConfigProvider)

message.config({
  top: '100px',
  duration: 2,
  maxCount: 2,
})

Vue.prototype.$message = message
Vue.prototype.$confirm = Modal.confirm
Vue.prototype.$info = Modal.info
Vue.prototype.$success = Modal.success
Vue.prototype.$error = Modal.error
Vue.prototype.$warning = Modal.warning
Vue.prototype.$eventBus = new Vue()

Vue.component(
  'AIconFont',
  Icon.createFromIconfontCN({
    scriptUrl: `${publicPath}/fonts/iconfont/iconfont.js`,
  })
)

Vue.config.productionTip = false
window.addEventListener('load', () => {
  new Vue({
    router,
    store,
    render: (h) => h(App),
  }).$mount('#app')
})
