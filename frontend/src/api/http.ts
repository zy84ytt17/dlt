/**
 * request base axios
 * @author zhangfanfan
 */

import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'
import { message } from 'ant-design-vue'
import { baseURL } from '@/api/axios-config'

const publicPath: string = process.env.BASE_URL
const disableMessageErrorCode: Set<number> = new Set([416, 417]) // 不需要提示信息的 error code

message.config({
  top: '100px',
  duration: 2,
  maxCount: 1,
})

const instance = axios.create({
  baseURL,
  method: 'post',
  headers: {
    post: {
      'Content-Type': 'application/json;charset=utf-8',
    },
    'Access-Control-Allow-Origin': '*',
  },
})

// Add a request interceptor
instance.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    // Do something before request is sent
    return config
  },
  (error) => {
    // Do something with request error
    message.error('请求超时:', error.toString())
    return Promise.reject(error)
  }
)

// Add a response interceptor
instance.interceptors.response.use(
  (response: AxiosResponse) => {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    if (disableMessageErrorCode.has(response.data.code)) {
      // 不需要提示信息的
      return response
    }
    switch (response.data.code) {
      case 200:
        return response
      default:
        return response
    }
  },
  (error) => {
    // is cancel request error
    if (axios.isCancel(error)) {
      console.warn('cancel request error:', error.message)
      return new Promise(() => {}) // 中断 promise 链接
    }
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    console.error(error.response)
    message.error(error.response.data.message)

    // 未登录
    if (error.response.data.code === 301) {
      window.location.href = `${publicPath}${
        publicPath.endsWith('/') ? '' : '/'
      }login`
    }
    return Promise.reject(error)
  }
)

export default instance
