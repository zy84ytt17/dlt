import axios from '@/api/http'

// 测试数据库
export const connectTestApi = (options?: any) =>
  axios.request({
    ...options,
    url: '/connectTest',
  })

// 获取所有数据库名
export const getAllDatabaseApi = (options?: any) =>
  axios.request({
    ...options,
    url: '/getAllDatabase',
  })

// 获取所有数据模式名
export const getAllSchemaApi = (options?: any) =>
  axios.request({
    ...options,
    url: '/getAllSchema',
  })

// 解析表、字段血缘
export const parseAllLineageApi = (options?: any) =>
  axios.request({
    ...options,
    url: '/parseAllLineage',
  })
