package org.zjvis.dlt.service;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.zjvis.dp.data.lineage.data.GrammarCheckErrorResult;
import org.zjvis.dp.data.lineage.parser.DataLineageParser;

/**
 * @author zhouyu
 * @create 2023-07-14 10:41
 */
@Service
public class DataLineageService {
    @Resource
    private DataLineageParser dataLineageParser;

    public GrammarCheckErrorResult grammarCheck(String sqlType, String sql) {
        return dataLineageParser.grammarCheck(sqlType, sql);
    }
}
