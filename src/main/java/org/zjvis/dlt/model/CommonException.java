package org.zjvis.dlt.model;

import lombok.Getter;
import org.zjvis.dlt.enums.ApiResultCode;

/**
 * @author hengzhang
 * @description
 * @create 2022/1/6
 */
@Getter
public class CommonException extends RuntimeException {

    private static final long serialVersionUID = 4465566270019332160L;

    private ApiResult apiResult;

    public CommonException(String message) {
        super(message);
        this.apiResult = ApiResult.valueOf(ApiResultCode.SYS_ERROR, message);
    }

    public CommonException(String message, Throwable cause) {
        super(message, cause);
        this.apiResult = ApiResult.valueOf(ApiResultCode.SYS_ERROR, message);
    }

    public CommonException(Throwable cause) {
        super(cause);
        this.apiResult = ApiResult.valueOf(ApiResultCode.SYS_ERROR);
    }

    public CommonException(ApiResultCode code, String message) {
        super(message);
        this.apiResult = ApiResult.valueOf(code, message);
    }
}
