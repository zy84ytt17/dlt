package org.zjvis.dlt.utils;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @author zhouyu
 * @create 2023-07-11 17:08
 */
public class DataLineageUtil {

    /**
     * 根据特定值去重
     * @param keyExtractor
     * @return
     * @param <T>
     */
    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

    public static boolean onlySemicolonsOrNewLine(String str) {
        for(int i = 0; i < str.length(); i++) {
            if(str.charAt(i) != '\n' && str.charAt(i) != ';') {
                return false;
            }
        }
        return true;
    }
}