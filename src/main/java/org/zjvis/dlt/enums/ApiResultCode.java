package org.zjvis.dlt.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zhouyu
 * @create 2023-07-07 15:33
 */
@Getter
@AllArgsConstructor
public enum ApiResultCode {

    SUCCESS(200, "成功!"),
    GRAMMAR_ERROR(300, "语法错误"),

    SYS_ERROR(500, "服务端异常");

    private int code;

    private String message;
}
