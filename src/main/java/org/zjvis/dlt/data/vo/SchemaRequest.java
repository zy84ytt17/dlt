package org.zjvis.dlt.data.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author zhouyu
 * @create 2023-07-10 14:30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
@ApiModel(description = "获取模式名请求参数")
public class SchemaRequest extends DatabaseRequest {

    @ApiModelProperty(value = "库名", required = true)
    private String databaseName;
}
