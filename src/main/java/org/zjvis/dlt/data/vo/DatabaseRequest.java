package org.zjvis.dlt.data.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author zhouyu
 * @create 2023-07-07 15:14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
@ApiModel(description = "数据库请求参数")
public class DatabaseRequest {
    @ApiModelProperty(value = "数据库类型,包括CLICKHOUSE、MYSQL、POSTGRES", required = true)
    private String sqlType;

    @ApiModelProperty(value = "主机ip", required = true)
    private String host;

    @ApiModelProperty(value = "端口", required = true)
    private Integer port;

    @ApiModelProperty(value = "用户名", required = true)
    private String username;

    @ApiModelProperty(value = "密码", required = true)
    private String password;
}
