package org.zjvis.dlt.data.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author zhouyu
 * @create 2023-07-11 16:05
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
@ApiModel(description = "数据血缘请求参数")
public class DataLineageRequest {

    @ApiModelProperty(value = "数据库信息")
    private DatabaseInfo databaseInfo;

    @ApiModelProperty(value = "sql语句")
    private String sql;
}
