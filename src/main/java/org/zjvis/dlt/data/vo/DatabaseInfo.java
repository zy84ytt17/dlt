package org.zjvis.dlt.data.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author zhouyu
 * @create 2023-07-11 16:04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
@ApiModel(description = "数据库信息")
public class DatabaseInfo {
    @ApiModelProperty(value = "数据库类型,包括CLICKHOUSE、MYSQL、POSTGRES", required = true)
    private String sqlType;

    @ApiModelProperty(value = "主机ip")
    private String host;

    @ApiModelProperty(value = "端口")
    private Integer port;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "数据库名")
    private String databaseName;

    @ApiModelProperty(value = "模式名")
    private String schemaName;

}
