package org.zjvis.dlt.data.data_lineage;

import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;

/**
 * @author zhouyu
 * @create 2023-07-11 16:18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
@ApiModel(value = "表血缘画布中边信息")
public class TableEdgeInfo{

    @ApiModelProperty(value = "边的源端，即表")
    private TableNodeInfo source;

    @ApiModelProperty(value = "边的末端，即目标表")
    private TableNodeInfo target;

    public String generateIdentity() {
        return StringUtils.join(
                Lists.newArrayList(
                        source.getDatabaseName(),
                        source.getTableName(),
                        target.getDatabaseName(),
                        target.getTableName()
                )
                , "_"
        );
    }
}