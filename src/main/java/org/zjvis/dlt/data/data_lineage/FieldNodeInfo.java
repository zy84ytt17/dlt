package org.zjvis.dlt.data.data_lineage;

import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;

/**
 * @author zhouyu
 * @create 2023-07-11 16:27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ApiModel(value = "字段血缘画布中节点信息")
public class FieldNodeInfo {
    @ApiModelProperty(value = "库名")
    private String databaseName;

    @ApiModelProperty(value = "表名")
    private String tableName;

    @ApiModelProperty(value = "表字段列表")
    private List<TableFieldDetail> tableFieldDetailList;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @SuperBuilder
    @ApiModel(value = "表字段详情")
    public static class TableFieldDetail{
        @ApiModelProperty(value = "字段名")
        private String fieldName;

        @ApiModelProperty(value = "字段类型")
        private String fieldType;
    }

    public String generateIdentity() {
        return StringUtils.join(
                Lists.newArrayList(
                        databaseName,
                        tableName
                )
                , "_"
        );
    }

}


