package org.zjvis.dlt.data.data_lineage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author zhouyu
 * @create 2023-07-11 16:31
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "字段详细信息")
public class FieldDetail extends TableNodeInfo {

    @ApiModelProperty(value = "字段名")
    private String fieldName;
}
