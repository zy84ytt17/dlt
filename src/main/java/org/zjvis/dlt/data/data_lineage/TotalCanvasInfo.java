package org.zjvis.dlt.data.data_lineage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhouyu
 * @create 2023-07-12 11:18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(value = "总画布信息")
public class TotalCanvasInfo {

    @ApiModelProperty(value = "表级别血缘画布信息")
    private TableLineageCanvasInfo tableLineageCanvasInfo;

    @ApiModelProperty(value = "字段级别血缘画布信息")
    private FieldLineageCanvasInfo fieldLineageCanvasInfo;
}
