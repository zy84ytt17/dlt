package org.zjvis.dlt.data.data_lineage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhouyu
 * @create 2023-07-11 16:12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(value = "表血缘画布信息")
public class TableLineageCanvasInfo {
    @ApiModelProperty(value = "节点信息列表")
    private List<TableNodeInfo> tableNodeInfoList;

    @ApiModelProperty(value = "边信息列表")
    private List<TableEdgeInfo> tableEdgeInfoList;
}
