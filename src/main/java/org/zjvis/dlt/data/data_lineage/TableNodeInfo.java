package org.zjvis.dlt.data.data_lineage;

import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;

/**
 * @author zhouyu
 * @create 2023-07-11 16:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ApiModel(value = "表血缘节点信息")
public class TableNodeInfo {

    @ApiModelProperty(value = "所属库名")
    protected String databaseName;

    @ApiModelProperty(value = "所属表名")
    protected String tableName;

    public String generateIdentity() {
        return StringUtils.join(
                Lists.newArrayList(
                        databaseName,
                        tableName
                )
                , "_"
        );
    }

}
