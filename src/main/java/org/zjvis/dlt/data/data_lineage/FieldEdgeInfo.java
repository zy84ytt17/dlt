package org.zjvis.dlt.data.data_lineage;

import com.google.common.collect.Lists;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;

/**
 * @author zhouyu
 * @create 2023-07-11 16:22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@ApiModel(value = "字段血缘画布中节点信息")
public class FieldEdgeInfo {
    @ApiModelProperty(value = "边的源端，即源字段")
    private FieldDetail source;

    @ApiModelProperty(value = "边的末端，即目标字段")
    private FieldDetail target;

    public String generateIdentity() {
        return StringUtils.join(
                Lists.newArrayList(
                        source.getDatabaseName(),
                        source.getTableName(),
                        source.getFieldName(),
                        target.getDatabaseName(),
                        target.getTableName(),
                        target.getFieldName()
                )
                , "_"
        );
    }
}
